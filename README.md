## ADE VSCode

- This project generates a Visual Studio Code (https://code.visualstudio.com/) for [`ade`](https://gitlab.com/ApexAI/ade-cli)

### How to use

- In the `.aderc` file add, `registry.gitlab.com/apexai/ade-vscode:latest` to the list of `ADE_IMAGES`: e.g.

```
export ADE_IMAGES="
  registry.gitlab.com/group/project/ade:latest
  registry.gitlab.com/apexai/ade-vscode:latest
"
```

### Getting a specific VSCode version

- Generating a volume for a different VSCode version is as simple as creating a git tag (e.g `v1.35.0`)
- [Create an issue](https://gitlab.com/ApexAI/ade-vscode/issues/new), if the desired version is not available 
